#ifndef DIOSA_3LAB_ROTATE_H
# define DIOSA_3LAB_ROTATE_H
#include "struct.h"

int rotate(struct image *new_img, const struct image *old_img);
#endif
