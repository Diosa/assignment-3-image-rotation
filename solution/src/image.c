#include <stdlib.h>

#include "struct.h"

void image_init(struct image *img){
    img->data = NULL;
    img->height = 0;
    img->width = 0;
}

int image_data_set_size(struct image *img, size_t new_height, size_t new_width) {
    if (img->data == NULL) {
        img->data = malloc(new_height * new_width * sizeof(struct pixel));
        img->height = new_height;
        img->width = new_width;
    } else {
        if (new_height * new_width > img->height * img->width) {
            img->data = realloc(img->data, new_height * new_width * sizeof(struct pixel));
            img->height = new_height;
            img->width = new_width;
        } else {
            return 0;
        }
    }

    if (img->data != NULL) {
        return 0;
    } else {
        img->width = 0;
        img->height = 0;
        return 1;
    }
}

void image_deinit(struct image *img){
    free(img->data);
    img->width = 0;
    img->height = 0;
}
