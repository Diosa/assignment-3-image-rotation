#ifndef DIOSA_3LAB_BMP_H
# define DIOSA_3LAB_BMP_H
#include "struct.h"
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum parse_bmp_result {
    parse_bmp_OK,
    parse_bmp_NOT_ENOUGH_BYTES_FOR_HEADER,
    parse_bmp_TYPE_IS_NOT_VALID,
    parse_bmp_RESERVED_IS_NOT_VALID,
    parse_bmp_SIZE_IS_NOT_VALID,
    parse_bmp_WIDTH_IS_NOT_VALID,
    parse_bmp_HEIGHT_IS_NOT_VALID,
    parse_bmp_PLANES_IS_NOT_VALID,
    parse_bmp_BITCOUNT_IS_NOT_VALID,
    parse_bmp_COMPRESSION_IS_NOT_VALID,
    parse_bmp_CLRIMPORTANT_IS_NOT_VALID,
    parse_bmp_XPELSPERMETER_IS_NOT_VALID,
    parse_bmp_YPELSPERMETER_IS_NOT_VALID,
    parse_bmp_NOT_ENOUGH_BYTES_FOR_IMAGE,
    parse_bmp_NOT_ENOUGH_MEMORY_FOR_IMAGE,
};

enum parse_bmp_result parse_bmp(struct byte_array *start_array, struct image *old_image);

int dump_bmp(struct image *new_img, struct byte_array *finish_array);
#endif
