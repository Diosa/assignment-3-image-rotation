#include <stdlib.h>

#include "struct.h"

void byte_array_init(struct byte_array *arr){
    arr->data = NULL;
    arr->capacity = 0;
}

int byte_array_extend(struct byte_array *arr, size_t new_capacity){
    if (arr->data == NULL) {
        arr->data = malloc(new_capacity);
        arr->capacity = new_capacity;
    } else {
        if (arr->capacity < new_capacity) {
            arr->data = realloc(arr->data, new_capacity);
            arr->capacity = new_capacity;
        }
    }

    if (arr->data != NULL) {
        return 0;
    } else {
        arr->capacity = 0;
        return 1;
    }
}

void byte_array_deinit(struct byte_array *arr){
    free(arr->data);
    arr->capacity = 0;
}
