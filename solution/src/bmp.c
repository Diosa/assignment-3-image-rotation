#include <stdint.h>
#include <string.h>

#include "bmp.h"
#include "struct.h"
#define HEADER_SIZE 54

enum parse_bmp_result parse_image(struct bmp_header *header, struct byte_array *start_array, struct image *old_img);

enum parse_bmp_result parse_bmp(struct byte_array *start_array, struct image *old_image){
    struct bmp_header header;
    if (start_array->capacity < HEADER_SIZE) {
        return parse_bmp_NOT_ENOUGH_BYTES_FOR_HEADER;
    }

#   define parse_bytes_16(FIELD, OFFSET)                         \
        (header.FIELD =                                          \
        (uint16_t) (start_array->data[(OFFSET) + 1]) << 8 |                 \
        (uint16_t) start_array->data[(OFFSET)])
#   define parse_bytes_32(FIELD, OFFSET)                         \
        (header.FIELD =                                          \
        (uint32_t)(start_array->data[(OFFSET) + 3]) << 24 |                  \
        (uint32_t)(start_array->data[(OFFSET) + 2]) << 16 |                  \
        (uint32_t)(start_array->data[(OFFSET) + 1]) << 8 |                   \
        (uint32_t)(start_array->data[(OFFSET)]))
    parse_bytes_16(bfType, 0);
    if (header.bfType != 0x4D42) {
        return parse_bmp_TYPE_IS_NOT_VALID;
    }
    parse_bytes_32(bfileSize, 2);
    parse_bytes_32(bfReserved, 6);
    if (header.bfReserved != 0) {
        return parse_bmp_RESERVED_IS_NOT_VALID;
    }
    parse_bytes_32(bOffBits, 10);
    parse_bytes_32(biSize, 14);
    if (header.biSize != 40) {
        return parse_bmp_SIZE_IS_NOT_VALID;
    }
    parse_bytes_32(biWidth, 18);
    if (header.biWidth <= 0) {
        return parse_bmp_WIDTH_IS_NOT_VALID;
    }
    parse_bytes_32(biHeight, 22);
    if (header.biHeight == 0) {
        return parse_bmp_HEIGHT_IS_NOT_VALID;
    }
    parse_bytes_16(biPlanes, 26);
    if (header.biPlanes != 1) {
        return parse_bmp_PLANES_IS_NOT_VALID;
    }
    parse_bytes_16(biBitCount, 28);
    if (header.biBitCount != 24) {
        return parse_bmp_BITCOUNT_IS_NOT_VALID;
    }
    parse_bytes_32(biCompression, 30);
    if (header.biCompression != 0) {
        return parse_bmp_COMPRESSION_IS_NOT_VALID;
    }
    parse_bytes_32(biSizeImage, 34);
    parse_bytes_32(biXPelsPerMeter, 38);
    if (header.biXPelsPerMeter < 0) {
        return parse_bmp_XPELSPERMETER_IS_NOT_VALID;
    }
    parse_bytes_32(biYPelsPerMeter, 42);
    if (header.biYPelsPerMeter < 0) {
        return parse_bmp_YPELSPERMETER_IS_NOT_VALID;
    }
    parse_bytes_32(biClrUsed, 46);
    parse_bytes_32(biClrImportant, 50);
    if (header.biClrImportant < 0) {
        return parse_bmp_CLRIMPORTANT_IS_NOT_VALID;
    }

    return parse_image(&header, start_array, old_image);
}

size_t len_calculate(uint32_t width_in_pixels) {
    return (size_t) width_in_pixels * 3 + width_in_pixels % 4;
}

enum parse_bmp_result parse_image(struct bmp_header *header, struct byte_array *start_array, struct image *old_img){
    size_t len = len_calculate(header->biWidth); // ширина с выравниванием
    size_t i;


    if (start_array->capacity < len * header->biHeight + header->bOffBits) {
        return parse_bmp_NOT_ENOUGH_BYTES_FOR_IMAGE;
    }

    if (image_data_set_size(old_img, header->biHeight, header->biWidth) != 0) {
        return parse_bmp_NOT_ENOUGH_MEMORY_FOR_IMAGE;
    }

    for (size_t y = 0; y < old_img->height; y++){
        i = HEADER_SIZE + len * y;
        for (size_t x = 0; x < old_img->width; x++) {
            old_img->data[y * old_img->width + x].b = start_array->data[i + 3 * x];
            old_img->data[y * old_img->width + x].g = start_array->data[i + 3 * x + 1];
            old_img->data[y * old_img->width + x].r = start_array->data[i + 3 * x + 2];
        }
    }
    return parse_bmp_OK;
}

void dump_image(struct image *new_img, struct byte_array *finish_array){
    size_t len = len_calculate(new_img->width); // ширина с выравниванием
    size_t i;
    static const struct padding_t {
        unsigned char bytes[3];
    } padding = {{0, 0, 0}};
    (void) padding.bytes;

    for (size_t y = 0; y < new_img->height; y++) {
        i = HEADER_SIZE + len * y;
        *((struct padding_t *) &(finish_array->data[i + len - sizeof (struct padding_t)])) = padding;
        for (size_t x = 0; x < new_img->width; x++) {
            finish_array->data[i + 3 * x] = new_img->data[y * new_img->width + x].b;
            finish_array->data[i + 3 * x + 1] = new_img->data[y * new_img->width + x].g;
            finish_array->data[i + 3 * x + 2] = new_img->data[y * new_img->width + x].r;
        }
    }
}

int dump_bmp(struct image *new_img, struct byte_array *finish_array){
    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = HEADER_SIZE + (new_img->width * 3 + new_img->width % 4) * new_img->height,
            .bfReserved = 0,
            .bOffBits = HEADER_SIZE,
            .biSize = 40,
            .biWidth = new_img->width,
            .biHeight = new_img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = (new_img->width * 3 + new_img->width % 4) * new_img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    if (byte_array_extend(finish_array, header.bfileSize) != 0) {
        return 1;
    }



#   define dump_bytes_16(FIELD, OFFSET)                                \
        finish_array->data[(OFFSET) + 1] = (header.FIELD & 0x0000ff00) >> 8;                 \
        finish_array->data[(OFFSET)] = header.FIELD & 0x000000ff
#   define dump_bytes_32(FIELD, OFFSET)                                \
        finish_array->data[(OFFSET) + 3] = (header.FIELD & 0xff000000) >> 24;             \
        finish_array->data[(OFFSET) + 2] = (header.FIELD & 0x00ff0000) >> 16;             \
        finish_array->data[(OFFSET) + 1] = (header.FIELD & 0x0000ff00) >> 8;             \
        finish_array->data[(OFFSET)] = header.FIELD & 0x000000ff
    dump_bytes_16(bfType, 0);
    dump_bytes_32(bfileSize, 2);
    dump_bytes_32(bfReserved, 6);
    dump_bytes_32(bOffBits, 10);
    dump_bytes_32(biSize, 14);
    dump_bytes_32(biWidth, 18);
    dump_bytes_32(biHeight, 22);
    dump_bytes_16(biPlanes, 26);
    dump_bytes_16(biBitCount, 28);
    dump_bytes_32(biCompression, 30);
    dump_bytes_32(biSizeImage, 34);
    dump_bytes_32(biXPelsPerMeter, 38);
    dump_bytes_32(biYPelsPerMeter, 42);
    dump_bytes_32(biClrUsed, 46);
    dump_bytes_32(biClrImportant, 50);

    dump_image(new_img, finish_array);

    return 0;
}

