#include <stdint.h>
#include <stdlib.h>

#ifndef DIOSA_3LAB_STRUCT_H
# define DIOSA_3LAB_STRUCT_H
struct byte_array {
    unsigned char *data;
    size_t capacity;
};
void byte_array_init(struct byte_array *arr);
int byte_array_extend(struct byte_array *arr, size_t new_capacity);
void byte_array_deinit(struct byte_array *arr);


struct image {
    uint64_t width, height;
    struct pixel *data;
};
void image_init(struct image *img);
int image_data_set_size(struct image *img, size_t new_height, size_t new_width);
void image_deinit(struct image *img);


struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};
void pixel_init(struct pixel *pxl);
#endif
