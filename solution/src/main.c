#include <stdio.h>

#include "bmp.h"
#include "file.h"
#include "rotate.h"
#include "struct.h"


int main(int argc, char **argv){
    struct byte_array start_array;
    struct image old_image;
    struct image new_image;
    struct byte_array finish_array;

    printf("Hello! Welcome to the program for rotating your picture!\n");

    if (argc != 3 ){
        fprintf(stderr, "Exactly two file paths expected\n");
        return 1;
    }

    byte_array_init(&start_array);

    switch (read_file(argv[1], &start_array)) {
        case read_file_result_READ_OK:
            printf("File read successfully\n");
            break;
        case read_file_result_NOT_OPENED:
            fprintf(stderr, "Failed to open file\n");
            byte_array_deinit(&start_array);
            return 1;
        case read_file_result_SEEK_FAULT:
            fprintf(stderr, "Failed to move through a file\n");
            byte_array_deinit(&start_array);
            return 1;
        case read_file_result_ALLOC_FAULT:
            fprintf(stderr, "No memory for reading file\n");
            byte_array_deinit(&start_array);
            return 1;
        case read_file_result_READ_FAULT:
            fprintf(stderr, "Failed to read file\n");
            byte_array_deinit(&start_array);
            return 1;
        case read_file_result_NOT_CLOSED:
            fprintf(stderr, "Failed to close file\n");
            byte_array_deinit(&start_array);
            return 1;
    }

    image_init(&old_image);

    switch (parse_bmp(&start_array, &old_image)) {

        case parse_bmp_OK:
            printf("BMP parsed successfully\n");
            break;
        case parse_bmp_NOT_ENOUGH_BYTES_FOR_HEADER:
            fprintf(stderr, "Invalid BMP header: not enough bytes for header\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_TYPE_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: type of bmp is not valid\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_RESERVED_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: the byte for reserved is not equal to 0\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_SIZE_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: image size is not valid\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_WIDTH_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: image width is not valid\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_HEIGHT_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: image height is not valid\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_PLANES_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: planes is not equal to 1\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_BITCOUNT_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: the bit count is not equal to 24\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_COMPRESSION_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: the compression byte is not equal to 0\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_CLRIMPORTANT_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: biClrImportant < 0\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_XPELSPERMETER_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: biXPelsPerMeter < 0\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_YPELSPERMETER_IS_NOT_VALID:
            fprintf(stderr, "Invalid BMP header: biYPelsPerMeter < 0\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_NOT_ENOUGH_BYTES_FOR_IMAGE:
            fprintf(stderr, "Invalid BMP body: not enough bytes for image\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
        case parse_bmp_NOT_ENOUGH_MEMORY_FOR_IMAGE:
            fprintf(stderr, "No memory for parsing image\n");
            byte_array_deinit(&start_array);
            image_deinit(&old_image);
            return 1;
    }


    byte_array_deinit(&start_array);
    image_init(&new_image);

    if (rotate(&new_image, &old_image) != 0) {
        fprintf(stderr, "Rotation failed :(\n");
        byte_array_deinit(&start_array);
        image_deinit(&old_image);
        image_deinit(&new_image);
        return 1;
    } else {
        printf("Rotated successfully :)\n");
    }

    image_deinit(&old_image);

    byte_array_init(&finish_array);

    if (dump_bmp(&new_image, &finish_array) != 0) {
        fprintf(stderr, "BMP Dumping failed\n");
        image_deinit(&new_image);
        byte_array_deinit(&finish_array);
        return 1;
    } else {
        printf("BMP dumped successfully\n");
    }

    image_deinit(&new_image);

    switch (write_file(argv[2], &finish_array)) {

        case write_file_result_WRITE_OK:
            printf("Rotated picture saved successfully\n");
            break;
        case write_file_result_NOT_OPENED:
            fprintf(stderr, "Failed to open file for saving\n");
            byte_array_deinit(&finish_array);
            return 1;
        case write_file_result_WRITE_FAULT:
            fprintf(stderr, "Failed to save the picture\n");
            byte_array_deinit(&finish_array);
            return 1;
        case write_file_result_NOT_CLOSED:
            fprintf(stderr, "Failed to close file for saving\n");
            byte_array_deinit(&finish_array);
            return 1;
    }

    byte_array_deinit(&finish_array);
    return 0;
}
