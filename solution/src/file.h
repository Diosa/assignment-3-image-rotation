#ifndef DIOSA_3LAB_FILE_H
# define DIOSA_3LAB_FILE_H
#include "struct.h"
enum read_file_result {
    read_file_result_READ_OK,
    read_file_result_NOT_OPENED,
    read_file_result_SEEK_FAULT,
    read_file_result_ALLOC_FAULT,
    read_file_result_READ_FAULT,
    read_file_result_NOT_CLOSED
};

enum read_file_result read_file(char *path, struct byte_array *array);

enum write_file_result {
    write_file_result_WRITE_OK,
    write_file_result_NOT_OPENED,
    write_file_result_WRITE_FAULT,
    write_file_result_NOT_CLOSED
};

enum write_file_result write_file(char *path, struct byte_array *array);
#endif
