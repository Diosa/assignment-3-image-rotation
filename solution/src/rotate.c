#include "rotate.h"
#include "struct.h"

int rotate(struct image *new_img, const struct image *old_img){
    if (image_data_set_size(new_img, old_img->width, old_img->height) != 0) {
        return 1;
    }

    for (int index = 0; index < old_img->width * old_img->height; index++) {
        unsigned int old_index = index;
        unsigned int old_y = old_index / old_img->width;
        unsigned int old_x = old_index % old_img->width;

        unsigned int new_y = old_x;
        unsigned int new_x = old_img->height - old_y - 1;

        unsigned int new_index = new_img->width * new_y + new_x;
        new_img->data[new_index] = old_img->data[old_index];
    }

    return 0;
}
