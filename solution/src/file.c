#include <stdio.h>

#include "file.h"
#include "struct.h"

enum read_file_result read_file(char *path, struct byte_array *array){
    FILE *file;

    file = fopen(path, "rb");
    if (file == NULL) {
        return read_file_result_NOT_OPENED;
    }

    // Смещаем указатель на конец файла
    if (fseek(file, 0, SEEK_END) != 0) {
        fclose(file);
        return read_file_result_SEEK_FAULT;
    }

    // Выделяем память для байтового массива
    if (byte_array_extend(array, ftell(file)) != 0){
        fclose(file);
        return read_file_result_ALLOC_FAULT;
    }

    // Смещаем указатель на начало файла
    if (fseek(file, 0, SEEK_SET) != 0) {
        fclose(file);
        return read_file_result_SEEK_FAULT;
    }

    if (fread(array->data, 1, array->capacity, file) != array->capacity){
        fclose(file);
        return read_file_result_READ_FAULT;
    }

    if (fclose(file) != 0){
        return read_file_result_NOT_CLOSED;
    }

    return read_file_result_READ_OK;
}

enum write_file_result write_file(char *path, struct byte_array *array){
    FILE *file;

    file = fopen(path, "wb");
    if (file == NULL) {
        return write_file_result_NOT_OPENED;
    }

    if (fwrite(array->data, 1, array->capacity, file) != array->capacity){
        fclose(file);
        return write_file_result_WRITE_FAULT;
    }

    if (fclose(file) != 0){
        return write_file_result_NOT_CLOSED;
    }
    return write_file_result_WRITE_OK;
}
